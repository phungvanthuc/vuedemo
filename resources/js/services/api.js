import axios from 'axios'
import qs from 'querystring'
axios.defaults.withCredentials = true

export const get = (uri, data = {}, headers = {}) => {
    return axios({
        method: 'get',
        url: uri,
        withCredentials: true
    })
}

export const methodDelete = (uri, data = {}, headers = {}) => {
    return axios({
        method: 'delete',
        url: uri,
        withCredentials: true
    })
}

export const post = (uri, data = {}, headers = {}) => {
    return axios({
        method: 'post',
        url: uri,
        data: qs.stringify(data),
        withCredentials: true
    })
}

export const put = (uri, data = {}, headers = {}) => {
    return axios({
        method: 'put',
        url: uri,
        data: qs.stringify(data),
        withCredentials: true
    })
}



export default {
    get,
    post,
    put,
    methodDelete
}
