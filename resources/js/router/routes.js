import UpdateUsersComponent from '../pages/users/UpdateUsersComponent'
import UsersComponent from '../pages/users/UsersComponent'

export default [{
    path: '/users/:id/edit',
    component: UpdateUsersComponent,
    name: 'UpdateUsersComponent',
    meta: {
        permission: []
    }
},
{
    path: '/users',
    component: UsersComponent,
    name: 'UsersComponent',
    meta: {
        permission: []
    }
}]
