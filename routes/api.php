<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
        Route::get('users', 'UserController@index')->name('users');
        Route::post('users/create', 'UserController@create')->name('createUser');
        Route::post('users/{id}/edit', 'UserController@update')->name('editUser');
        Route::get('users/{id}', 'UserController@show')->name('usersDetail');
});
