<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Resources\{
    UsersCollection,
    UsersResource
};

class UserController extends Controller
{
    /**
     * @var User
     */
    protected $_user;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->_user = $user;
    }

    /**
     * @return UsersCollection
     */
    public function index()
    {
        $users = $this->_user->all();

        return new UsersCollection($users);
    }

    /**
     * @param Request $request
     * @return UsersResource
     */
    public function create(Request $request)
    {
        try {
            $user = $this->_user->create([
                "name" => $request->name,
                "email" => $request->email,
                "password" => $request->password,
            ]);
        } catch (\Exception $e) {
            abort(500);
        }

        return new UsersResource($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->_user->find($id);

        return new UsersResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->_user->find($id);
        $user->update([
            "name" => $request->name,
            "email" => $request->email,
            "password" => $request->password,
        ]);

        return new UsersResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
